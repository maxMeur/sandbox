import { useState } from "react";
import { useTranslation } from "react-i18next";
import { Input } from "shared/ui/Input/Input";

const Main = () => {
    const {t} = useTranslation("main");
    const [value, setValue] = useState<string>('');

    const onChange = (val: string) => {
        setValue(val);
    }

    return <>
        <h1>
            {t("main")}
        </h1>
        <Input value={value} onChange={onChange} placeholder={"Enter text"}/>
    </>
}

export default Main;