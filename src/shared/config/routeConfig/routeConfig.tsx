import { AboutPage } from "pages/AboutPage"
import { MainPage } from "pages/MainPage"
import { NotFoundPage } from "pages/NotFoundPage"
import { RouteProps } from "react-router-dom"

export enum ROUTES {
    MAIN = 'main',
    ABOUT = 'about',
    NOT_FOUND = 'not_found',
}

export const RoutePath: Record<ROUTES, string> = {
    [ROUTES.MAIN]: '/',
    [ROUTES.ABOUT]: '/about',
    [ROUTES.NOT_FOUND]: '*',
}

export const routeConfig: Record<ROUTES, RouteProps> = {
    [ROUTES.MAIN]: {
        path: RoutePath.main,
        element: <MainPage />
    },
    [ROUTES.ABOUT]: {
        path: RoutePath.about,
        element: <AboutPage />
    },
    [ROUTES.NOT_FOUND]: {
        path: RoutePath.not_found,
        element: <NotFoundPage />
    }
}