import { classNames } from "shared/lib/classNames/classNames";
import { InputHTMLAttributes, memo, useEffect, useRef, useState } from "react";
import * as cls from './Input.module.scss';

type HTMLInputProps = Omit<InputHTMLAttributes<HTMLInputElement>, 'onChange'>

interface InputProps extends HTMLInputProps {
    className?: string;
    value?: string;
    onChange?: (value: string) => void;
    autoFocus?: boolean;
}

export const Input = memo((props: InputProps) => {

    const {className, value, onChange, type="text", placeholder, autoFocus, ...otherProps} = props;
    const [isFocus, setIsFocused] = useState(false);
    const [isMounted, setIsMounted] = useState(false);
    const [caretPosition, setCaretPosition] = useState(0);
    const ref = useRef<HTMLInputElement>();

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange?.(e.target.value);
        setCaretPosition(e.target.value.length);
    }

    const onBlur = () => {
        setIsFocused(false);
    }

    const onFocus = () => {
        setIsFocused(true);
    }

    const onSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCaretPosition(e.target.selectionStart || 0)
    }

    useEffect(() => {
        if (autoFocus) {
            setIsFocused(true);
            ref.current?.focus()
        }
    }, [autoFocus]);

    return (
        <div className={classNames(cls.InputWrapper, {}, [className])}>
            {placeholder && <div className={cls.placeholder}>
                {`${placeholder}:`}
            </div>}
            <div className={cls.caretWrapper}>
                <input
                    ref={ref}
                    type={type}
                    value={value}
                    onChange={onChangeHandler}
                    className={cls.input}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onSelect={onSelect}
                    {...otherProps}
                />
                {isFocus && <span className={cls.caret} style={{left: `${caretPosition * 7}px`}}></span>}
            </div>
        </div>
    )
})