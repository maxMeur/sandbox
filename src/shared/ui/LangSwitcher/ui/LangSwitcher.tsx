import { useTranslation } from "react-i18next"
import { classNames } from "shared/lib/classNames/classNames";
import * as cls from './LangSwitcher.module.scss';
import { Button, ButtonTheme } from "shared/ui/Button/Button";

import UKFlag from 'shared/assets/icons/uk.svg';
import RUFlag from 'shared/assets/icons/ru.svg';

interface LangSwitcherProps {
    className?: string;
    short: boolean;
}

export const LangSwitcher = ({className, short}: LangSwitcherProps) => {
    const {i18n} = useTranslation();

    const toggleLang = () => {
        i18n.changeLanguage(i18n.language === 'ru' ? 'en' : 'ru');
    }

    return (
        <Button className={classNames(cls.LangSwitcher, {}, [className])} theme={ButtonTheme.CLEAR} onClick={toggleLang}>
        {
            // t("changeLang")
            i18n.language === 'ru' ? <RUFlag /> : <UKFlag />
        }
        {
            !short && <span>{i18n.language === 'ru' ? "Русский язык" : "English language"}</span>
        }
        </Button>
    )
}   