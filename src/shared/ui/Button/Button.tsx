import { classNames } from 'shared/lib/classNames/classNames';
import { ButtonHTMLAttributes, FC, ReactNode } from 'react';
import * as cls from './Button.module.scss';

export enum ButtonTheme {
    CLEAR = 'clear',
    OUTLINE = 'outline',
    BACKGROUND = 'background',
    BACKGROUND_INVERTED = 'backgroundInverted',
}

export enum ButtonSize {
    M = 'size_m',
    L = 'size_l',
    XL = 'size_xl'
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    className?: string;
    theme?: ButtonTheme;
    children: ReactNode;
    square?: boolean;
    size?: ButtonSize;
}

export const Button: FC<ButtonProps> = (props) => {

    const {className, children,  theme, square, size = ButtonSize.M, ...otherProps} = props;

    return (
        <button
        data-testid="custom-button"
        type="button"
        className={classNames(cls.Button, {[cls.square]: square, [cls[size]]: size}, [className, cls[theme]])}
        {...otherProps}>
            {children}
        </button>
    )
}