import { fireEvent, render, screen } from '@testing-library/react';
import { Button, ButtonTheme } from 'shared/ui/Button/Button';
import * as cls from './Button.module.scss';

describe("Test Button", () => {
    test("Check if button rendered", () => {
        render(<Button>Test1</Button>);
        
        expect(screen.getByText('Test1')).toBeInTheDocument();
    });

    test("Check if class is clear", () => {
        render(<Button theme={ButtonTheme.CLEAR}>Test2</Button>);

        expect(screen.getByTestId("custom-button")).toHaveClass(cls.clear);
    });
})