import { classNames } from "shared/lib/classNames/classNames";

describe('classnames', () => {
    test('with only one parameter', () => {
        expect(classNames('someClassName')).toBe('someClassName');
    });

    test('with additional class', () => {
        expect(classNames('someClassName', {}, ['class1'])).toBe('someClassName class1');
    });

    test('with mods', () => {
        expect(classNames('someClassName', {hovered: true, scrollable: true}, [])).toBe('someClassName hovered scrollable');
    })

    test('with false mods', () => {
        expect(classNames('someClassName', {hovered: true, scrollable: false}, [])).toBe('someClassName hovered');
    })

    test('with undefined mods', () => {
        expect(classNames('someClassName', {hovered: true, scrollable: undefined}, [])).toBe('someClassName hovered');
    })
})