import { classNames } from "shared/lib/classNames/classNames";
import * as cls from './NavBar.module.scss';
import { FC, useCallback, useState } from "react";
import { AppLink, AppLinkTheme } from "shared/ui/AppLink/AppLink";
import { useTranslation } from "react-i18next";
import { RoutePath } from "shared/config/routeConfig/routeConfig";

import Home from 'shared/assets/icons/home.svg';
import About from 'shared/assets/icons/about.svg';
import { Button, ButtonTheme } from "shared/ui/Button/Button";
import { LoginModal } from "features/AuthByUserName";

interface NavBarProps {
    className?: string
}

export const NavBar: FC<NavBarProps> = ({className}) => {
    const {t} = useTranslation();

    const [isAuthModal, setIsAuthModal] = useState(false);

    const onCloseModal = useCallback(() => {
        setIsAuthModal(false);
    }, []);

    const onOpenModal = useCallback(() => {
        setIsAuthModal(true);
    }, [])

    return (
        <div className={classNames(cls.Navbar, {}, [className])}>
            <div className={cls.links}>
                <AppLink theme={AppLinkTheme.SECONDARY} to={RoutePath.main}>
                    {
                        <Home className={cls.icon} />
                    }
                    {
                        <span>{t("main")}</span>
                    }
                </AppLink>
                <AppLink theme={AppLinkTheme.SECONDARY} to={RoutePath.about}>
                    {
                        <About className={cls.icon}/>
                    }
                    {
                        <span>{t("about")}</span>
                    }
                </AppLink>
                <Button className={cls.links} theme={ButtonTheme.OUTLINE} onClick={onOpenModal}>{t("signin")}</Button>
                <LoginModal isOpen={isAuthModal} onClose={onCloseModal} />
            </div>
        </div>
    )
}