import { renderWithTranslation } from "shared/lib/tests/renderWithTranslation/renderWithTranslation"
import { SideBar } from "./SideBar"
import { fireEvent, screen } from '@testing-library/react';
import * as cls from './SideBar.module.scss';

describe("SideBar", () => {
    test('with only first param', () => {
        renderWithTranslation(<SideBar />);
        expect(screen.getByTestId('sidebar')).toBeInTheDocument();
    });

    test('test toggle', () => {
        renderWithTranslation(<SideBar/>);
        
        const toggleBtn = screen.getByTestId('sidebar-toggle');

        fireEvent.click(toggleBtn);
        expect(screen.getByTestId('sidebar')).toHaveClass(cls.isHide);
        
        fireEvent.click(toggleBtn);
        expect(screen.getByTestId('sidebar')).not.toHaveClass(cls.isHide);
    });
})