import { classNames } from 'shared/lib/classNames/classNames';
import * as cls from './SideBar.module.scss';
import { useState } from 'react';
import { ThemeSwitcher } from 'shared/ui/ThemeSwitcher';
import { LangSwitcher } from 'shared/ui/LangSwitcher';
import { Button, ButtonSize, ButtonTheme } from 'shared/ui/Button/Button';

interface SideBarProps {
    className?: string;
}

export const SideBar = ({className}: SideBarProps) => {

    const [isHide, setIsHide] = useState(false);

    const onToggle = () => {
        setIsHide(prev => !prev);
    }

    return (
        <div data-testid="sidebar" className={classNames(cls.SideBar, {[cls.isHide]: isHide}, [className])}>
            <Button 
                theme={ButtonTheme.BACKGROUND_INVERTED}
                data-testid="sidebar-toggle"
                square
                size={ButtonSize.L}
                className={cls.isHideBtn} onClick={onToggle}>
                    {isHide ? ">" : "<"}
            </Button>
            <div className={cls.switchers}>
                <LangSwitcher short={isHide}/>
                <ThemeSwitcher />
            </div>
        </div>
    )
}