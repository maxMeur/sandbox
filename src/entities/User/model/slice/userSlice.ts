import { createSlice } from "@reduxjs/toolkit";
import { UserSchema } from "../types/user";

const initialState: UserSchema = {
    authData: {
        id: "1",
        username: "bob"
    }
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {

    }
});

export const {actions: userActions} = userSlice;
export const {actions: userReducer} = userSlice;