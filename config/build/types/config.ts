export type ModeOption = 'production' | 'development';

export interface BuildPaths {
    entry: string;
    build: string;
    html: string;
    src: string;
}

export interface BuildEnv {
    mode: ModeOption;
    port: number;
}

export interface BuildOptions {
    mode: ModeOption;
    paths: BuildPaths;
    isDev: boolean;
    port: number;
}