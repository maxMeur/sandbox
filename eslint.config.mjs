import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginReactConfig from "eslint-plugin-react/configs/recommended.js";
import { fixupConfigRules } from "@eslint/compat";
import eslintReact from 'eslint-plugin-react';
import eslintReactHooks from 'eslint-plugin-react-hooks';
import eslintReactRefresh from 'eslint-plugin-react-refresh';
import prettierPlugin from 'eslint-plugin-prettier';
import eslintConfigPrettier from 'eslint-config-prettier';

import { dirname } from 'path';

export default tseslint.config(
    {
        plugins: {
            '@typescript-eslint': tseslint.plugin,
            'react-hooks': eslintReactHooks,
            'react-refresh': eslintReactRefresh,
            prettier: prettierPlugin,
            eslintReact,
        },
    },
    {languageOptions: { globals: {
        ...globals.browser,
        ...globals.node,
        ...globals.es2021
    },
    parserOptions: {
        // project: ['./tsconfig.json'],
        // tsconfigRootDir: dirname
        parser: '@typescript-eslint/parser',
        project: './tsconfig.json',
        tsconfigRootDir: dirname
    },
    }},
    pluginJs.configs.recommended,
    ...tseslint.configs.recommended,
    ...fixupConfigRules(pluginReactConfig),
    {
        rules: {
            // suppress errors for missing 'import React' in files
            "react/react-in-jsx-scope": "off",
            // allow jsx syntax in js files (for next.js project)
            "react/jsx-filename-extension": [2, { "extensions": [".ts", ".tsx"] }],
            "react/jsx-indent": [2, 4],
            "indent": [2,4],
            "react/jsx-indent-props": [2,4],
            "import/no-unresolved": "off",
            "import/prefer-default-export": "off",
            "no-unused-vars": "warn",
            "react/require-default-props": "off",
            "react/function-component-definition": "off",
            "@typescript-eslint/no-unused-vars": "warn",
            "react-hooks/rules-of-hooks": "error",
            "react-hooks/exhaustive-deps": "error",
            "react/display-name": "off"
        }
    },
    {
        ignores: ['node_modules', 'dist']
    },
    {
        files: ['**/*{ts,tsx}']
    },
    eslintConfigPrettier,
);